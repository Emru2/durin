# WEŹ TO ZRÓB JAK CZŁOWIEK

from abc import ABC
from operator import itemgetter
import requests


class MoriaMigrator(ABC):
    MORIA_URL = 'http://moria.umcs.lublin.pl/api'

    @classmethod
    def get_activities_for_students(cls, id):
        '''
        Metoda łączy się z API Morii, pobiera dane o podanym roczniku i zwraca
        w postaci trzech kontenerów.

        :param id: id rocznika
        :returns
            events: lista tupli (moria_id, start_time, end_time, length,
        weekday, room_id, teacher_moria_id, group_id)
            subjects: set tupli (name, moria_subject_id, type, groups)
            teachers: set tupli (moria_teacher_id, degree, first_name,
        last_name)

        '''
        url = f'{cls.MORIA_URL}/activity_list_for_students'
        response = requests.get(url, json={'id': id}).json()
        events = list()
        subjects = set()
        teachers = set()
        print(response['status'])
        if response['status'] == 'ok':
            for activity in response['result']['array']:
                event_array = itemgetter('event_array')(activity)[0]
                current_subject = itemgetter('subject', 'subject_id')(activity)
                current_subject += itemgetter('name')(activity['type']),
                current_event = itemgetter('id', 'start_time', 'end_time',
                                           'length', 'weekday',
                                           'room_id')(event_array)
                current_teacher_id, current_teacher_full_name = itemgetter(
                    'id', 'name')(activity['teacher_array'][0])
                current_teacher_full_name = current_teacher_full_name.rsplit(
                    ' ', 2)
                current_event += current_teacher_id,
                students_array = activity['students_array']
                students_info = list(
                    filter(lambda students_object: students_object['id'] == id,
                           students_array))[0]
                current_group, groups = itemgetter('group', 'groups')(
                    students_info)  # aktualna_grupa
                current_event += current_group,
                current_subject += groups,
                subjects.add(current_subject)
                events.append(current_event)
                teachers.add((current_teacher_id, current_teacher_full_name[0],
                              current_teacher_full_name[-2],
                              current_teacher_full_name[-1]))

            return events, subjects, teachers


if __name__ == '__main__':
    id = int(input('Podaj id rocznika: '))
    e, s, t = MoriaMigrator.get_activities_for_students(id)

    print('Lista aktywnosci: ')
    print("{:<10} {:<15} {:<15} {:<10} {:<10} {:<15} {:<18} {:<30}".format(
        'moria_id', 'start_time', 'end_time', 'length', 'weekday', 'room_id',
        'teacher_moria_id', 'group_id'))
    for event in e:
        moria_id, start_time, end_time, length, weekday, room_id, teacher_moria_id, group_id = event
        print("{:<10} {:<15} {:<15} {:<10} {:<10} {:<15} {:<18} {:<30}".format(
            moria_id, start_time, end_time, length, weekday, room_id,
            teacher_moria_id, group_id))

    print('\nLista zajec(tematow): ')
    print("{:<60} {:<30} {:<30} {:<10}".format(
        'name',
        'moria_subject_id',
        'type',
        'groups',
    ))
    for subject in s:
        name, moria_subject_id, moria_type, groups = subject
        print("{:<60} {:<30} {:<30} {:<10}".format(name, moria_subject_id,
                                                   moria_type, groups))

    print('\nLista nauczycieli')
    print("{:<16} {:10} {:<30} {:<50}".format(
        'moria_teacher_id',
        'degree',
        'first_name',
        'last_name',
    ))
    for teacher in t:
        moria_teacher_id, degree, first_name, last_name = teacher
        print("{:<16} {:<10} {:<30} {:<50}".format(moria_teacher_id, degree,
                                                   first_name, last_name))
