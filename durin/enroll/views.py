from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from enroll.models import EnrollmentSession, UserEnroll
from enroll.serializers import EnrollmentSerializer, UserEnrollSerializer


@api_view(['GET'])
def get_available(request):
    enrollments = EnrollmentSession.objects.filter(active=True)
    serializer = EnrollmentSerializer(enrollments, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_enrolled(request):
    user = request.user
    user_enrollments = UserEnroll.objects.filter(user_id=user)
    enrollments = EnrollmentSession.objects.filter(
        id__in=user_enrollments.values_list('enroll_id'))
    serializer = EnrollmentSerializer(enrollments, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
def enroll_user(request):
    user = request.user
    serializer = UserEnrollSerializer(data=request.data,
                                      context={'user': user})
    data = {}
    if serializer.is_valid():
        serializer.save()
        data['message'] = 'User added to enrollment'
        return Response(data, status=status.HTTP_201_CREATED)
    else:
        data['message'] = 'Failed to add user'
        data['errors'] = serializer.errors
        return Response(data, status=status.HTTP_400_BAD_REQUEST)
