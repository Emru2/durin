from django.contrib import admin
from .models import EnrollmentSession, EnrollLink, UserEnroll

# Register your models here

admin.site.register(EnrollmentSession)
admin.site.register(EnrollLink)
admin.site.register(UserEnroll)