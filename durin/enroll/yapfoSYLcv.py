from django.db import models
from user.models import User


class EnrollmentSession(models.Model):
    '''
    Model reprezentujący sesję zapisów
    moria_id - id roku który ma zapisy, jest to też numer w morii
    active - czy zapisy są aktywne
    name_short - krótka nazwa, typu "2 rok Informatyki"
    name_long - dłuższa nazwa, typu 2020 2 rok Informatyki 2019"
    desc - opis który ląduje na głównej stronie zapisów dla danego roku,
     typu na co się mają zapisać, fakultety itp
    '''
    moria_id = models.IntegerField()
    active = models.BooleanField(default=False)
    name_short = models.TextField(default='')
    name_long = models.TextField(default='')
    desc = models.TextField(default='')


class StartTime(models.Model):
    '''
    Model ten reprezentuje czas otwarcia oraz zamknięcia zapisów
    enroll_id - id zapisów, model EnrollmentSession
    start_time - czas rozpoczęcia zapisów
    end_time - czas zakończenia zapisów
    '''
    enroll_id = models.OneToOneField(EnrollmentSession,
                                     on_delete=models.CASCADE,
                                     related_name='start_time',
                                     primary_key=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()


class EnrollLink(models.Model):
    '''
    Link do rejestracji na dane zapisy
    enroll_id - id zapisów, model EnrollmentSession
    token - token UUID4 umożliwiający zapis, podawany w linku
    active - czy dany token jest aktywny
    valid_until - do kiedy token jest ważny
    '''
    enroll_id = models.OneToOneField(EnrollmentSession,
                                     on_delete=models.CASCADE,
                                     related_name='enroll_link',
                                     primary_key=True)
    token = models.StringField(default='', max_length=40)
    active = models.BooleanField(default=False)
    valid_until = models.DateTimeField()


class UserEnroll(models.Model):
    '''
    Na jakie zapisy zapisał się użytkownik
    user_id - id użytkownika
    enroll_id - id zapisów
    '''
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKeyField(User,
                                     on_delete=models.CASCADE,
                                     related_name='user_enroll')
    enroll_id = models.OneToOneField(EnrollmentSession,
                                     on_delete=models.CASCADE,
                                     related_name='user_enroll')
