from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
import requests


class EnrollTestCase(APITestCase):
    def setUp(self) -> None:
        res = requests.post("http://172.7.5.10/api/user/login/", {"cn": "jan.kowalski", "password": "1234"})
        self.access_token = eval(res.content.decode())["access"]

    def test_available(self):
        response = requests.get("http://172.7.5.10/api/enroll/available/",
                                headers={"Authorization": f"Bearer {self.access_token}"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(type(eval(response.content.decode())), list)

    def test_get_enrolled(self):
        response = requests.get("http://172.7.5.10/api/enroll/enrolled/",
                                headers={"Authorization": f"Bearer {self.access_token}"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(type(eval(response.content.decode())), list)

    def test_get_enroll_invalid_id(self):
        response = requests.post("http://172.7.5.10/api/enroll/enroll/",
                                headers={"Authorization": f"Bearer {self.access_token}"},
                                data={"enroll_id": -1})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(eval(response.content.decode()),
                         {"message": "Failed to add user", "errors": {"enroll_id": ["Invalid EnrollmentSession ID"]}})

    def test_get_enroll_valid_id_not_active(self):
        response = requests.post("http://172.7.5.10/api/enroll/enroll/",
                                headers={"Authorization": f"Bearer {self.access_token}"},
                                data={"enroll_id": 1})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(eval(response.content.decode()),
                         {"message": "Failed to add user", "errors": {"enroll_id": ["Enrollment is not active"]}})