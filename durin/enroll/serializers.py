from rest_framework import serializers
from enroll.models import EnrollmentSession, UserEnroll


class EnrollmentSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    moria_id = serializers.IntegerField()
    name_short = serializers.CharField()
    name_long = serializers.CharField()
    desc = serializers.CharField()


class UserEnrollSerializer(serializers.Serializer):
    enroll_id = serializers.IntegerField()

    def validate_enroll_id(self, value):
        try:
            enroll = EnrollmentSession.objects.get(id=value)
        except EnrollmentSession.DoesNotExist:
            raise serializers.ValidationError('Invalid EnrollmentSession ID')
        if not enroll.active:
            raise serializers.ValidationError('Enrollment is not active')
        try:
            _ = UserEnroll.objects.get(enroll_id=enroll,
                                       user_id=self.context['user'])
        except UserEnroll.DoesNotExist:
            return value
        else:
            raise serializers.ValidationError(
                'Given user has already been registered')

    def save(self):
        enroll = EnrollmentSession.objects.get(
            id=self.validated_data['enroll_id'])
        userenroll = UserEnroll.objects.create(user_id=self.context['user'],
                                               enroll_id=enroll)
        userenroll.save()
