from django.urls import path
from enroll.views import (
    get_available,
    get_enrolled,
    enroll_user,
)

urlpatterns = [
    path('available/', get_available, name='get available'),
    path('enrolled/', get_enrolled, name='get enrolled'),
    path('enroll/', enroll_user, name='enroll user'),
]
