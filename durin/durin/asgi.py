"""
ASGI config for durin project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

from django.core.asgi import get_asgi_application
application = get_asgi_application()

import os
import enlist.routing

from enlist.tokenauth import JWTTokenAuthMiddleware
from channels.routing import ProtocolTypeRouter, URLRouter

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'durin.settings')

application = ProtocolTypeRouter({
    "http":
    get_asgi_application(),
    "websocket":
    JWTTokenAuthMiddleware(URLRouter(enlist.routing.websocket_urlpatterns)),
})
