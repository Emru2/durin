# Generated by Django 3.1.3 on 2020-11-24 18:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activity', '0002_auto_20201124_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='degree',
            field=models.CharField(default='', max_length=32),
        ),
        migrations.AddField(
            model_name='teacher',
            name='first_name',
            field=models.CharField(default='', max_length=256),
        ),
        migrations.AddField(
            model_name='teacher',
            name='last_name',
            field=models.CharField(default='', max_length=256),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
