from django.urls import path
from activity.views import (
    MigrateView,
    ScheduleView,
    ActivitiesView,
)

app_name = 'activity'

urlpatterns = [
    path('migrate/', MigrateView.as_view(), name='migrate'),
    path(
        'schedule/<str:id>/',
        ScheduleView.as_view(),
        name='get schedule view'
    ),
    path('activities/', ActivitiesView.as_view(), name='get activities view'),
]
