# Create your tests here.
from rest_framework import status
from rest_framework.test import APITestCase
import requests


class ActivityTestCase(APITestCase):
    def setUp(self) -> None:
        res = requests.post("http://172.7.5.10/api/user/login/", {"cn": "jan.kowalski", "password": "1234"})
        self.access_token = eval(res.content.decode())["access"]

    def test_activities(self):
        response = requests.get("http://172.7.5.10/api/activity/activities/",
                           headers={"Authorization": f"Bearer {self.access_token}"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(type(eval(response.content.decode())), list)

    def test_migrate_valid_id(self):
        response = requests.get("http://172.7.5.10/api/activity/migrate/year/840/",
                                headers={"Authorization": f"Bearer {self.access_token}"})
        valid_dict = {
            "activities_list": [],
            "message": "done",
            "status": 200
        }
        res_dict = eval(response.content.decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res_dict.keys(), valid_dict.keys())
        self.assertEqual(res_dict["message"], valid_dict["message"])
        self.assertEqual(res_dict["status"], valid_dict["status"])
        self.assertTrue(len(res_dict["activities_list"]) > 0)

    def test_migrate_invalid_id(self):
        response = requests.get("http://172.7.5.10/api/activity/migrate/year/-1000/",
                                headers={"Authorization": f"Bearer {self.access_token}"})
        valid_dict = {
            "activities_list": [],
            "message": "done",
            "status": 200
        }
        res_dict = eval(response.content.decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res_dict.keys(), valid_dict.keys())
        self.assertEqual(res_dict["message"], valid_dict["message"])
        self.assertEqual(res_dict["status"], valid_dict["status"])
        self.assertTrue(len(res_dict["activities_list"]) == 0)