from rest_framework import serializers

from activity.models import Subject, Activity, Teacher
from enlist.models import Enlist
from enroll.models import EnrollmentSession


class PlainActivitySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    subject_id = serializers.IntegerField(source='subject_id_id')
    room_id = serializers.IntegerField(source='room_id_id')
    teacher_id = serializers.IntegerField(source='teacher_id_id')
    start_time = serializers.TimeField()
    end_time = serializers.TimeField()
    group = serializers.IntegerField()
    week_day = serializers.IntegerField()
    max_students = serializers.IntegerField()
    locked_in = serializers.BooleanField()
    locked_out = serializers.BooleanField()


class TeacherSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    degree = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()


class RoomSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()


class TypeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()


class SubjectSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    groups = serializers.IntegerField()
    type = TypeSerializer(source='type_id')

    class Meta:
        Model = Teacher


class ScheduleActivitiesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    teacher = TeacherSerializer(source='teacher_id')
    room = RoomSerializer(source='room_id')
    subject = SubjectSerializer(source='subject_id')
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField()
    locked_in = serializers.BooleanField()
    locked_out = serializers.BooleanField()
    max_students = serializers.IntegerField()
    enlisted_students = serializers.SerializerMethodField()

    def get_enlisted_students(self, obj):
        num = Enlist.objects.filter(activity_id=obj, flee=False).count()
        return num


class ActivityForStudentSerializer(serializers.ModelSerializer):
    teacher = TeacherSerializer(read_only=True, source='teacher_id')
    room = RoomSerializer(read_only=True, source='room_id')
    subject = SubjectSerializer(read_only=True, source='subject_id')

    class Meta:
        model = Activity
        fields = ['id', 'subject', 'teacher', 'room',
                  'start_time', 'end_time', 'week_day', 'group']


class ScheduleListSerializer(serializers.Serializer):
    day = serializers.CharField(max_length=16)
    begin = serializers.DateTimeField()
    end = serializers.DateTimeField()
    activities = ScheduleActivitiesSerializer(many=True)


class ScheduleSerializer(serializers.Serializer):
    days = serializers.SerializerMethodField()
    generated = serializers.DateTimeField()

    def validate(self, data):
        print(data)
        try:
            _ = EnrollmentSession.objects.get(id=data['enr_id'])
        except EnrollmentSession.DoesNotExist:
            raise serializers.ValidationError("Wrong ID")
        return data

    def get_days(self, obj):
        enroll_ses = EnrollmentSession.objects.get(id=obj['enr_id'])
        subjects = Subject.objects.filter(enroll_id=enroll_ses.id)
        activities = Activity.objects.filter(subject_id__in=subjects)
        activities_dict = [dict() for i in range(7)]
        for i in range(7):
            activities_dict[i]['day'] = i + 1
            activities_dict[i]['activities'] = activities.filter(week_day=i +
                                                                 1)
            if not activities_dict[i]['activities']:
                activities_dict[i]['begin'] = None
                activities_dict[i]['end'] = None
            else:
                activities_dict[i]['begin'] = activities_dict[i][
                    'activities'].first().start_time
                activities_dict[i]['end'] = activities_dict[i][
                    'activities'].first().end_time

            for act in activities_dict[i]['activities']:
                if act.start_time < activities_dict[i]['begin']:
                    activities_dict[i]['begin'] = act.start_time

                if act.end_time > activities_dict[i]['end']:
                    activities_dict[i]['end'] = act.end_time
        serializer = ScheduleListSerializer(activities_dict, many=True)
        return serializer.data


class IdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
