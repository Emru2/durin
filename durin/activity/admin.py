from django.contrib import admin

from .models import Activity, Type, Subject, Room, Teacher

# Register your models here

admin.site.register(Activity)
admin.site.register(Type)
admin.site.register(Subject)
admin.site.register(Room)
admin.site.register(Teacher)
