from django.db import models
from enroll.models import EnrollmentSession


class Type(models.Model):
    '''
    Typ przedmiotu
    name - nazwa
    '''
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256, default='')

    def __int__(self):
        return self.id

    def __str__(self):
        return f'{self.name}'


class Subject(models.Model):
    '''
    Model przedmiotu
    name - nazwa przedmiotu, duża na wypadek ogłoszeń w planie
    groups - ilosc grup, pobierana z morii
    enroll_id - ID zapisów
    type_id - ID typu przedmiotu (wykład, ćwiczenia, etc.)
    lecture_related - FK do powiązanego wykładu. Przy zapisach na ćwiczenia
        z fakultetu od razu zapisuje na ćwiczenia. Należy ustawić to ręcznie
        w bazie
    '''
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=1024, default='')
    groups = models.IntegerField()
    enroll_id = models.ForeignKey(EnrollmentSession,
                                  on_delete=models.CASCADE,
                                  related_name='subject_related')
    type_id = models.ForeignKey(Type,
                                on_delete=models.CASCADE,
                                related_name='subject_related')
    lecture_related = models.ForeignKey('Activity',
                                        on_delete=models.SET_NULL,
                                        related_name='exer_related',
                                        null=True,
                                        blank=True,
                                        default=None)

    def __int__(self):
        return self.id

    def __str__(self):
        if self.lecture_related:
            return f'{self.name} - wyklad: {self.lecture_related.subject.name}'
        return self.name


class Room(models.Model):
    '''
    Model pomieszczenia do którego przypisane są zajęcia
    name - nazwa
    '''
    name = models.CharField(max_length=256, default='')
    id = models.IntegerField(primary_key=True)

    def __int__(self):
        return self.id

    def __str__(self):
        return self.name


class Teacher(models.Model):
    '''
    Model nauczyciela
    '''
    id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=256, default='')
    last_name = models.CharField(max_length=256, default='')
    degree = models.CharField(max_length=32, default='')

    def __int__(self):
        return self.id

    def __str__(self):
        return f'{self.degree} {self.first_name} {self.last_name}'


class Activity(models.Model):
    '''
    Model aktywności, konkretne zajęcia na planie
    subject_id - ID przedmiotu
    room_id - ID pomieszczenia
    teacher_id - ID nauczyciela
    start_time - czas startu danej aktywności
    end_time - czas końca danej aktywności
    group - numer grupy danej aktywności, pobierane z Morii
    week_day - dzień tygodnia
    max_students - maksymalna liczba zapisanych studentów
    locked_in - czy zapisy są zablokowane
    locked_out - czy wypisywanie się jest zablokowane
    '''
    id = models.IntegerField(primary_key=True)
    subject_id = models.ForeignKey(Subject,
                                   on_delete=models.CASCADE,
                                   related_name='activity_related')
    room_id = models.ForeignKey(Room,
                                on_delete=models.CASCADE,
                                related_name='activity_related')
    teacher_id = models.ForeignKey(Teacher,
                                   on_delete=models.CASCADE,
                                   related_name='activity_related')
    start_time = models.TimeField()
    end_time = models.TimeField()
    group = models.IntegerField()
    week_day = models.IntegerField()
    max_students = models.IntegerField(default=10)
    locked_in = models.BooleanField(default=False)
    locked_out = models.BooleanField(default=False)

    def __int__(self):
        return self.id

    def __str__(self):
        return f'{self.subject_id}'
