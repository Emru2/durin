import requests
from datetime import datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser

from activity.models import Teacher, Room, Subject, Type, Activity
from enroll.models import EnrollmentSession
from enlist.models import Enlist

from activity.serializers import (
    ScheduleSerializer,
    ActivityForStudentSerializer,
    IdSerializer,
)


class MigrateView(APIView):
    permission_classes = [IsAdminUser]
    serializer_class = IdSerializer

    def post(self, request):
        s = self.serializer_class(data=request.data)
        if s.is_valid():
            id = s.validated_data['id']
            teachers_list = requests.get(
                url='http://moria.umcs.lublin.pl/api/teacher_list').json(
                )['result']['array']
            for teacher in teachers_list:
                _ = Teacher.objects.update_or_create(
                    first_name=teacher['first_name'],
                    last_name=teacher['last_name'],
                    degree=teacher['degree'],
                    id=teacher['id']
                )
            print('teachers migrated')

            rooms_list = requests.get(
                url='http://moria.umcs.lublin.pl/api/room_list'
            ).json()['result']['array']
            for room in rooms_list:
                _ = Room.objects.update_or_create(
                    id=room['id'],
                    name=room['name'],
                )
            print('rooms migrated')

            years_list = requests.get(
                url='http://moria.umcs.lublin.pl/api/students_list').json(
                )['result']['array']

            for year in years_list:
                _ = EnrollmentSession.objects.update_or_create(
                    moria_id=year['id'],
                    name_short=year['name'],
                )

            activities_list = requests.get(
                url='http://moria.umcs.lublin.pl/api/activity_list_for_students',  # noqa: E501
                json={
                    "id": id
                }).json()['result']['array']

            for activity in activities_list:
                for event in activity['event_array']:
                    for teacher in activity['teacher_array']:
                        for students in activity['students_array']:
                            xtype, _ = \
                                Type.objects.update_or_create(
                                    id=activity['type']['id'],
                                    name=activity['type']['name'])
                            subject, _ = \
                                Subject.objects.update_or_create(
                                    id=activity['subject_id'],
                                    enroll_id=EnrollmentSession.objects.get(
                                        moria_id=id
                                    ),
                                    groups=students['groups'],
                                    name=activity['subject'],
                                    type_id=xtype)

                            activity_enc, _ = \
                                Activity.objects.update_or_create(
                                    id=event['id'],
                                    week_day=event['weekday'],
                                    teacher_id=Teacher.objects.get(
                                        id=teacher['id']
                                    ),
                                    room_id=Room.objects.get(
                                        id=event['room_id']
                                    ),
                                    subject_id=subject,
                                    start_time=event['start_time'],
                                    end_time=event['end_time'],
                                    group=students['group'])

            print('Types, subjects and activities migrated')

            return Response(
                {
                    'activities_list': activities_list,
                    'message': 'done',
                    'status': 200,
                },
                status=200
            )
        return Response(
            {
                'message': s.errors,
                'status': 400,
            },
            status=400
        )


class ScheduleView(APIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = ScheduleSerializer

    def get(self, request, id):
        print(id)
        now = datetime.now()
        enr_id = int(id)
        try:
            enroll_ses = EnrollmentSession.objects.get(id=enr_id)
        except EnrollmentSession.DoesNotExist:
            return Response({"message": "No such schedule ID"})
        subjects = Subject.objects.filter(enroll_id=enroll_ses.id)
        activities = Activity.objects.filter(subject_id__in=subjects)
        activities_dict = [dict() for i in range(7)]
        for i in range(7):
            activities_dict[i]['day'] = i + 1
            activities_dict[i]['activities'] = activities.filter(week_day=i +
                                                                 1)
            if not activities_dict[i]['activities']:
                activities_dict[i]['begin'] = None
                activities_dict[i]['end'] = None
            else:
                activities_dict[i]['begin'] = activities_dict[i][
                    'activities'].first().start_time
                activities_dict[i]['end'] = activities_dict[i][
                    'activities'].first().end_time

            for act in activities_dict[i]['activities']:
                if act.start_time < activities_dict[i]['begin']:
                    activities_dict[i]['begin'] = act.start_time

                if act.end_time > activities_dict[i]['end']:
                    activities_dict[i]['end'] = act.end_time
        serializer = self.serializer_class(
            {
                'days': activities_dict,
                'generated': now
            }
        )
        return Response(serializer.data)


class ActivitiesView(APIView):
    serializer_class = ActivityForStudentSerializer

    def get(self, request):
        enroll_ids = request.user.user_enroll.all()
        enlists = Enlist.objects.filter(user_enroll_id__in=enroll_ids)
        data = Activity.objects.filter(enlist_related__in=enlists)
        serializer = self.serializer_class(data, many=True)
        return Response(serializer.data)
