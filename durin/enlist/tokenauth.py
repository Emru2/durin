from django.contrib.auth.models import AnonymousUser
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import InvalidToken
from asgiref.sync import sync_to_async
import sys


class JWTTokenAuthMiddleware:
    """
    SimpleJWT Auth Channels middleware
    """
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):

        auth = JWTAuthentication()
        headers = (scope['headers'])
        for header in headers:
            if header[0].decode('utf-8') == 'authorization':
                raw_token = header[1].decode('utf-8').split()[1]
                raw_token = raw_token.encode('utf-8')
                break

        try:
            validated_token = auth.get_validated_token(raw_token)
            user = sync_to_async(auth.get_user)(validated_token)
            scope['user'] = await user
        except InvalidToken:
            scope['user'] = AnonymousUser

        return await self.app(scope, receive, send)
