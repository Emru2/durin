import json
from django.db.models import Q
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async

from datetime import datetime

from enlist.models import Enlist
from enroll.models import EnrollmentSession, UserEnroll

from activity.models import Activity
from activity.serializers import PlainActivitySerializer


class EnlistConsumer(AsyncWebsocketConsumer):
    def _related_enlist(self, act: Activity):
        if not self._check_collision(act):
            return False
        enlist = Enlist.objects.create(user_enroll_id=self.user_enroll,
                                       activity_id=act)
        enlist.save()
        return True

    def _related_flee(self, act: Activity):
        try:
            enlist = Enlist.objects.get(activity_id=act,
                                        flee=False,
                                        user_enroll_id=self.user_enroll)
            enlist.flee = True
            enlist.flee_time = datetime.now()
            enlist.save()
        except Enlist.DoesNotExist:
            pass

    def _check_collision(self, act: Activity):
        '''
        Funkcja sprawdza czy użytkownik ma jakieś kolizje z daną aktywnością
        Do wywołania tylko z funkcji synchronicznych
        '''
        q1 = Q(activity_id__end_time__gt=act.start_time) & Q(
            activity_id__start_time__lte=act.start_time)
        q2 = Q(activity_id__end_time__gte=act.end_time) & Q(
            activity_id__start_time__lt=act.end_time)
        q3 = Q(activity_id__end_time__lte=act.end_time) & Q(
            activity_id__start_time__gte=act.start_time)
        enl = Enlist.objects.filter(
            q1 | q2 | q3,
            user_enroll_id__enroll_id=self.enrollment_obj,
            flee=False,
            activity_id__week_day=act.week_day)
        if enl:
            return False
        return True

    @database_sync_to_async
    def _check_activity_id(self, id: int):
        '''
        Funkcja sprawdza, czy ID aktywności jest poprawne, czy taka aktywność
        istnieje oraz czy może się do niej zapisać z danego EnrollmentSession.

        :param int id: ID aktywności
        :return tuple(bool, string, Activity):
        '''
        try:
            act = Activity.objects.get(id=id)
        except Activity.DoesNotExist:
            return (False, "Given activity does not exist", None)
        subject = act.subject_id

        if self.enrollment_obj != subject.enroll_id:
            return (
                False,
                "Given activity does not belong to this enrollment session",
                act)

        return (True, "", act)

    @database_sync_to_async
    def _enlist_if_possible(self, act: Activity):
        '''
        Funkcja, jeśli to możliwe, zapisuje studenta do zajęć
        w przeciwnym wypadku zwraca błąd związany z przekroczonym limitem

        :param act Activity: Obiekt aktywności
        :return (bool, int): Powodzenie lub nie akcji oraz aktualna
                             liczba zapisanych studentów
        '''
        enlisted = Enlist.objects.filter(
            user_enroll_id__enroll_id=self.enrollment_obj,
            activity_id=act,
            flee=False)
        enlisted_num = enlisted.count()
        if act.locked_in:
            return (False, enlisted_num)
        if enlisted_num >= act.max_students:
            return (False, enlisted_num)

        try:
            '''Czy jest już zapisany na inne, takie same zajęcia'''
            _ = Enlist.objects.get(user_enroll_id=self.user_enroll,
                                   activity_id__subject_id=act.subject_id,
                                   flee=False)
        except Enlist.DoesNotExist:
            '''Czy są jakieś kolizje'''
            ret = self._check_collision(act)
            if ret:
                if act.subject_id.lecture_related:
                    ret = self._related_enlist(act.subject_id.lecture_related)
                if ret:
                    Enlist.objects.create(user_enroll_id=self.user_enroll,
                                          activity_id=act)
                return (ret, enlisted_num + 1)
            return (False, enlisted_num)

    @database_sync_to_async
    def _flee(self, act: Activity):
        '''
        Funkcja wypisze studenta z zajęć, jeśli jest na nie zapisany

        :param act Activity: Obiekt aktywności
        :return (bool, int): Powodzenie lub nie akcji oraz aktualna
                             liczba zapisanych studentów
        '''
        enlisted = Enlist.objects.filter(
            user_enroll_id__enroll_id=self.enrollment_obj,
            activity_id=act,
            flee=False)
        enlisted_num = enlisted.count()
        try:
            enl_user = enlisted.get(user_enroll_id=self.user_enroll)
        except Enlist.DoesNotExist:
            return (False, enlisted_num)
        if act.locked_out:
            return (False, enlisted_num)
        else:
            if act.subject_id.lecture_related:
                self._related_flee(act.subject_id.lecture_related)
            enl_user.flee = True
            enl_user.flee_time = datetime.now()
            enl_user.save()
            return (True, enlisted_num - 1)

    @database_sync_to_async
    def _check_connect(self):
        try:
            self.enrollment_obj = EnrollmentSession.objects.get(
                id=self.scope['url_route']['kwargs']['enroll_id'])
        except EnrollmentSession.DoesNotExist:
            return (False, "No such enrollment session ID")

        try:
            self.user_enroll = UserEnroll.objects.get(
                user_id=self.scope['user'], enroll_id=self.enrollment_obj)
        except UserEnroll.DoesNotExist:
            return (False, "User does not belong to this enrollment session")

        if not self.enrollment_obj.active:
            return (False, "This enrollment session is not active")

        return (True, "")

    async def connect(self):
        self.room_name = ''
        self.room_group_name = ''
        await self.accept()
        if self.scope['user'].is_anonymous:
            await self.send(
                json.dumps({
                    "type": "websocket.error",
                    "message": "User not logged in",
                }))
            await self.close()
            return

        rtlp = await self._check_connect()
        ret = rtlp[0]
        message = rtlp[1]
        if not ret:
            await self.send(
                json.dumps({
                    "type": "websocket.error",
                    "message": message,
                }))
            await self.close()

        self.room_name = self.scope['url_route']['kwargs']['enroll_id']
        self.room_group_name = 'enlist_%s' % self.room_name
        await self.channel_layer.group_add(self.room_group_name,
                                           self.channel_name)
        await self.send(
            json.dumps({
                "type": "message",
                "message": "Connected!",
            }))

    async def disconnect(self, close_code):
        if self.room_group_name and self.channel_name:
            await self.channel_layer.group_discard(self.room_group_name,
                                                   self.channel_name)
        await self.close()

    async def receive(self, text_data):
        error = False
        try:
            text_data_json = json.loads(text_data)
        except json.decoder.JSONDecodeError:
            error = True
        else:
            if ('type' not in text_data_json or 'enlist' not in text_data_json
                    or 'id' not in text_data_json):
                error = True
            elif not (type(text_data_json['type']) is str
                      and type(text_data_json['enlist']) is bool
                      and type(text_data_json['id']) is int):
                error = True
            elif text_data_json['type'] != "message":
                error = True

        if error:
            await self.send(
                json.dumps({
                    "type": "websocket.error",
                    "message": "Invalid message formating",
                }))
            return

        rtlp = await self._check_activity_id(text_data_json['id'])
        ret = rtlp[0]
        message = rtlp[1]
        act = rtlp[2]
        if not ret:
            await self.send(
                json.dumps({
                    "type": "websocket.error",
                    "message": message,
                }))
            return
        act_serializer = PlainActivitySerializer(act.__dict__)
        act_data = act_serializer.data
        if text_data_json['enlist']:
            rtlp = await self._enlist_if_possible(act)
            ret = rtlp[0]
            num = rtlp[1]
            if ret:
                await self.send(
                    json.dumps({
                        "type": "message",
                        "message": "Enlisted successfully",
                        "id": act.id,
                        "activity": act_data,
                        "enlisted": num,
                    }))
                await self.channel_layer.group_send(
                    self.room_group_name, {
                        "type": "update",
                        "message": "Activity update",
                        "id": act.id,
                        "activity": act_data,
                        "enlisted": num,
                    })
            else:
                await self.send(
                    json.dumps({
                        "type": "message",
                        "message": "Failed to enlist",
                        "id": act.id,
                        "activity": act_data,
                        "enlisted": num,
                    }))
        else:
            rtlp = await self._flee(act)
            ret = rtlp[0]
            num = rtlp[1]
            if ret:
                await self.send(
                    json.dumps({
                        "type": "message",
                        "message": "Fled successfully",
                        "id": act.id,
                        "activity": act_data,
                        "enlisted": num,
                    }))
                await self.channel_layer.group_send(
                    self.room_group_name, {
                        "type": "update",
                        "message": "Activity update",
                        "id": act.id,
                        "activity": act_data,
                        "enlisted": num,
                    })
            else:
                await self.send(
                    json.dumps({
                        "type": "message",
                        "message": "Failed to flee",
                        "id": act.id,
                        "activity": act_data,
                        "enlisted": num,
                    }))

    async def message(self, event):
        await self.send(
            json.dumps({
                "type": "message",
                "message": event['message'],
                "id": event['id'],
                "activity": event['activity'],
                "enlisted": event['enlisted'],
            }))

    async def update(self, event):
        await self.send(
            json.dumps({
                "type": "update",
                "message": event['message'],
                "id": event['id'],
                "activity": event['activity'],
                "enlisted": event['enlisted'],
            }))
