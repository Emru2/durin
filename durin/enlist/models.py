from django.db import models

from enroll.models import UserEnroll
from activity.models import Activity


class Enlist(models.Model):
    '''
    Model zapisu na zajęcia, razem z datą ucieczki
    user_enroll_id - ID zapisów konretnego użytkownika
    activity_id - ID aktywności
    enlisted_time - o jakim czasie ktoś się zapisał
    flee - czy został wypisany
    flee_time - data wypisania się
    '''
    id = models.AutoField(primary_key=True)
    user_enroll_id = models.ForeignKey(UserEnroll,
                                       on_delete=models.CASCADE,
                                       related_name='enlist_related')
    activity_id = models.ForeignKey(Activity,
                                    on_delete=models.CASCADE,
                                    related_name='enlist_related')
    enlisted_time = models.DateTimeField(auto_now_add=True)
    flee = models.BooleanField(default=False)
    flee_time = models.DateTimeField(blank=True, default=None, null=True)
