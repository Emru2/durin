from django.urls import path
from enlist.consumers import EnlistConsumer

websocket_urlpatterns = [
    path('ws/enlist/<int:enroll_id>/', EnlistConsumer.as_asgi()),
]
