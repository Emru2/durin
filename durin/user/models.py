import random
from datetime import datetime, timedelta
from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)


class UserManager(BaseUserManager):
    def create_user(self, legit, email, first_name, last_name, password):

        user = self.model(
            legit=legit,
            email=email,
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.act_token_exp = datetime.now() + timedelta(days=1)
        user.reset_token_exp = datetime.now() - timedelta(days=1)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_superuser(self, legit, password):
        user = self.create_user(
            legit=legit,
            email="kontakt@skni.umcs.pl",
            first_name="admin",
            last_name="admin",
            password=password
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    """
    Model representing user in database
    """
    id = models.AutoField(primary_key=True)
    email = models.EmailField(unique=True)
    legit = models.IntegerField(unique=True)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'legit'

    def __str__(self):
        return str(self.legit)
