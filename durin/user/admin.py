from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User

# Register your models here.
class CustomUserAdmin(UserAdmin):
    model = User
    list_display = ('legit', 'email', 'is_staff')
    search_fields = ('legit', 'email')
    readonly_fields = ('id', 'last_login')
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()
    add_fieldsets =  (
        (None, {
            'classes': ('wide', ),
            'fields': ('legit', 'email', 'password1', 'password2', 'is_admin', 'is_staff'),
        }),
    )
    ordering = ('legit', 'email')

admin.site.register(User, CustomUserAdmin)
