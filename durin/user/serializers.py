from rest_framework import serializers

from user.models import User


class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=256)
    first_name = serializers.CharField(max_length=128)
    last_name = serializers.CharField(max_length=128)
    legit = serializers.IntegerField()
    password = serializers.CharField(
        write_only=True,
        style={'input_type': 'password'}
    )

    def create(self):
        user = User.objects.create(
            email=self.validated_data['email'],
            legit=self.validated_data['legit'],
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
        )
        user.save()
        return user


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    legit = serializers.IntegerField()
