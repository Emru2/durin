from django.test import TestCase

# Create your tests here.

# Create your tests here.
from rest_framework import status
from rest_framework.test import APITestCase
import requests


class UserTestCase(APITestCase):
    def setUp(self) -> None:
        res = requests.post("http://172.7.5.10/api/user/login/", {"cn": "jan.kowalski", "password": "1234"})
        self.access_token = eval(res.content.decode())["access"]

    def test_register_created(self):
        response = requests.post("http://172.7.5.10/api/user/register/",
                                 data={"email": "michal.kowalski@mail.com"})

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(eval(response.content.decode()), {"message": "Created new user account"})

    def test_register_no_email_field(self):
        response = requests.post("http://172.7.5.10/api/user/register/",
                                 data={"blabla": "michal.kowalski@mail.com"})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(eval(response.content.decode()), {"message":{"email":["This field is required."]}})

    def test_register_invalid_email(self):
        response = requests.post("http://172.7.5.10/api/user/register/",
                                 data={"email": "michal.kowalski_mail.com"})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(eval(response.content.decode()), {"message":{"email":["Enter a valid email address."]}})

    def test_login(self):
        response = requests.post("http://172.7.5.10/api/user/login/", {"cn": "jan.kowalski", "password": "1234"})

        valid_dict= {
            "refresh": "",
            "access": ""
        }

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(eval(response.content.decode()).keys(), valid_dict.keys())
