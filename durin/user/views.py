from rest_framework import status
from rest_framework.response import Response

from rest_framework.views import APIView

from user.serializers import (
    RegisterSerializer,
    UserSerializer,
)


class RegisterView(APIView):
    authentication_classes = []
    permission_classes = []

    serializer = RegisterSerializer

    def post(self, request):
        s = self.serializer(data=request.data)
        data = {}
        if s.is_valid():
            user = s.create()
            data['message'] = "Created new user account"
            data['user'] = UserSerializer(user).data
            rstatus = status.HTTP_201_CREATED
        else:
            data['message'] = s.errors
            rstatus = status.HTTP_404_NOT_FOUND
        return Response(data, status=rstatus)


class MeView(APIView):
    serializer = UserSerializer

    def get(self, request):
        s = self.serializer(request.user)
        return Response(s.data)
