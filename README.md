# DURIN
---

## Description
---

This project aims to create a system which will be used by students to sign up
for classes. It's based on PostgreSQL database, Django with Django Rest
Framework and Django Channels on backend, React on frontend.

## Team:
---

* Emil Tomczyk: [JIRA](https://durin.atlassian.net/issues/?jql=assignee%20in%20(5f76c7a5a2909600750abd50)%20order%20by%20created%20DESC)
* Marcin Szyszka: [JIRA](https://durin.atlassian.net/issues/?jql=assignee%20in%20(5fa47ab78573800069c4725a)%20order%20by%20created%20DESC)
* Antoni Pietryga: [JIRA](https://durin.atlassian.net/issues/?jql=assignee%20in%20(5da0672a4ee5bb0c260d75e8)%20order%20by%20created%20DESC)
* Malika Wawer: [JIRA](https://durin.atlassian.net/issues/?jql=assignee%20in%20(5f9027a919376b007582a238, 5f802d91bd1298006f184463)%20order%20by%20created%20DESC)