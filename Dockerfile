FROM python:3.10-buster
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
ENV DJANGO_SETTINGS_MODULE=durin.settings
CMD [ "daphne", "-b", "0.0.0.0", "-p", "8001", "durin.asgi:application"]
